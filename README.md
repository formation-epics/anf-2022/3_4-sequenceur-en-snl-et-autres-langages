# 3_4 Séquenceur en SNL et autres langages

> 60 mn

Cette partie commencera par une présentation de 40 mn sur le langage SNL, elle sera suivie d'une
rapide démonstration de son intégration dans un IOC EPICS et enfin, nous évoquerons quelques
alternatives disponibles.

## Table des matières

<!-- vim-markdown-toc GitLab -->

* [Prérequis](#prérequis)
* [Références](#références)
* [Présentation](#présentation)
* [Démo SNL](#démo-snl)
* [Autres langages](#autres-langages)

<!-- vim-markdown-toc -->

## Prérequis

- [Rocky Linux 8](https://rockylinux.org/)
- ["2_3 base support extensions etc"](https://gitlab.com/formation-epics/anf-2022/2_1-base-support-extensions-etc)

## Références

- <https://controlssoftware.sns.ornl.gov/training/>
- <https://controlssoftware.sns.ornl.gov/training/2022_USPAS/#presentations>
- <https://controlssoftware.sns.ornl.gov/training/2022_USPAS/Presentations/14%20Sequencer.pdf>
- <https://controlssoftware.sns.ornl.gov/training/2022_USPAS/Presentations/14l%20SequencerLab.pdf>
- <https://controlssoftware.sns.ornl.gov/training/2019_USPAS/Presentations/11_Sequencer.pdf>
- <https://www-csr.bessy.de/control/SoftDist/sequencer/>
- <https://www-csr.bessy.de/control/SoftDist/sequencer/Tutorial.html>
- <https://epics-controls.org>
- <https://docs.epics-controls.org>
- <https://docs.epics-controls.org/en/latest/appdevguide/gettingStarted.html?highlight=sequencer#sequencer-example>
- <https://epics.anl.gov/index.php>
- <https://epics.anl.gov/modules/common.php>

## Présentation

> 40 mn

Cf. [présentation SNL](./presentation_snl.pdf) (basée sur
<https://controlssoftware.sns.ornl.gov/training/2022_USPAS/Presentations/14%20Sequencer.pdf>)

## Démo SNL

> 10 mn

Créez un top :
```console
$ cd $HOME
$ mkdir -p Tops/toto-top
$ cd Tops/toto-top
$ makeBaseApp.pl -a linux-x86_64 -t ioc toto && makeBaseApp.pl -a linux-x86_64 -i -t ioc -p toto toto
$ make clean && make && echo OK || echo KO
```

Créez un programme SNL vraiment très simple :

- On importe le module SEQ :
    ```console
    $ vi ./configure/RELEASE
        > # RELEASE - Location of external support modules
        > #
        > # IF YOU CHANGE ANY PATHS in this file or make API changes to
        > # any modules it refers to, you should do a "make rebuild" in
        > # this application's top level directory.
        > #
        > # The EPICS build process does not check dependencies against
        > # any files from outside the application, so it is safest to
        > # rebuild it completely if any modules it depends on change.
        > #
        > # Host- or target-specific settings can be given in files named
        > #  RELEASE.$(EPICS_HOST_ARCH).Common
        > #  RELEASE.Common.$(T_A)
        > #  RELEASE.$(EPICS_HOST_ARCH).$(T_A)
        > #
        > # This file is parsed by both GNUmake and an EPICS Perl script,
        > # so it may ONLY contain definititions of paths to other support
        > # modules, variable definitions that are used in module paths,
        > # and include statements that pull in other RELEASE files.
        > # Variables may be used before their values have been set.
        > # Build variables that are NOT used in paths should be set in
        > # the CONFIG_SITE file.
        >
        > # Variables and paths to dependent modules:
        > #MODULES = /path/to/modules
        > #MYMODULE = $(MODULES)/my-module
        >
        > # If using the sequencer, point SNCSEQ at its top directory:
        > #SNCSEQ = $(MODULES)/seq-ver
      + > SNCSEQ = /opt/epics/support/seq-2-2-9
        >
        > # EPICS_BASE should appear last so earlier modules can override stuff:
        > EPICS_BASE = /opt/epics/base-7.0-gcc
        >
        > # Set RULES here if you want to use build rules from somewhere
        > # other than EPICS_BASE:
        > #RULES = $(MODULES)/build-rules
        >
        > # These lines allow developers to override these RELEASE settings
        > # without having to modify this file directly.
        > -include $(TOP)/../RELEASE.local
        > -include $(TOP)/../RELEASE.$(EPICS_HOST_ARCH).local
        > -include $(TOP)/configure/RELEASE.local
    ```

- On ajoute deux records (histoire d'avoir des données avec lesquelles jouer) :
    ```console
    $ vi totoApp/Db/toto.db
      + > record(ao, "foo:plop")
      + > {
      + >     field(VAL, 42)
      + > }
      + > record(ao, "foo:plip")
      + > {
      + >     field(VAL, 23)
      + > }
    
    $ vi totoApp/Db/Makefile
        > TOP=../..
        > include $(TOP)/configure/CONFIG
        > #----------------------------------------
        > #  ADD MACRO DEFINITIONS AFTER THIS LINE
        >
        > #----------------------------------------------------
        > # Create and install (or just install) into <top>/db
        > # databases, templates, substitutions like this
        > #DB += xxx.db
      + > DB += toto.db
        >
        > #----------------------------------------------------
        > # If <anyname>.db template is not named <anyname>*.template add
        > # <anyname>_template = <templatename>
        >
        > include $(TOP)/configure/RULES
        > #----------------------------------------
        > #  ADD RULES AFTER THIS LINE
    ```

- On crée deux petits programmes SNL :
    ```console
    $ vi ./totoApp/src/totoseq.st
      + > program totoseq
      + > double v;
      + > assign v to "foo:{plip_ou_plop}";
      + > monitor v;
      + >
      + > ss ss1 {
      + >     state init {
      + >         when (delay(10)) {
      + >             printf("toto: Startup delay over\n");
      + >         } state low
      + >     }
      + >     state low {
      + >         when (v > 5.0) {
      + >             printf("toto: Changing to high\n");
      + >         } state high
      + >     }
      + >     state high {
      + >         when (v <= 5.0) {
      + >             printf("toto: Changing to low\n");
      + >         } state low
      + >     }
      + > }
    
    $ vi ./totoApp/src/tutuseq.st
      + > program tutuseq
      + > double v;
      + > assign v to "foo:{plip_ou_plop}";
      + > monitor v;
      + >
      + > ss ss1 {
      + >     state init {
      + >         when (delay(10)) {
      + >             printf("tutu: Startup delay over\n");
      + >         } state low
      + >     }
      + >     state low {
      + >         when (v > 50.0) {
      + >             printf("tutu: Changing to high\n");
      + >         } state high
      + >     }
      + >     state high {
      + >         when (v <= 50.0) {
      + >             printf("tutu: Changing to low\n");
      + >         } state low
      + >     }
      + > }
    ```

- On déclare ces programmes dans un nouveau `.dbd` dédié :
    ```console
    $ vi totoApp/src/seq.dbd
      + > registrar(totoseqRegistrar)
      + > registrar(tutuseqRegistrar)
    ```

- On spécifie le tout dans le Makefile associé :
    ```console
    $ vi totoApp/src/Makefile
        > TOP=../..
        >
        > include $(TOP)/configure/CONFIG
        > #----------------------------------------
        > #  ADD MACRO DEFINITIONS AFTER THIS LINE
        > #=============================
        >
        > #=============================
        > # Build the IOC application
        >
        > PROD_IOC = toto
        > # toto.dbd will be created and installed
        > DBD += toto.dbd
        >
        > # toto.dbd will be made up from these files:
        > toto_DBD += base.dbd
        >
        > # Include dbd files from all support applications:
        > #toto_DBD += xxx.dbd
        >
        > # Add all the support libraries needed by this IOC
        > #toto_LIBS += xxx
        >
        > # toto_registerRecordDeviceDriver.cpp derives from toto.dbd
        > toto_SRCS += toto_registerRecordDeviceDriver.cpp
        >
        > # Build the main IOC entry point on workstation OSs.
        > toto_SRCS_DEFAULT += totoMain.cpp
        > toto_SRCS_vxWorks += -nil-
        >
        > # Add support from base/src/vxWorks if needed
        > #toto_OBJS_vxWorks += $(EPICS_BASE_BIN)/vxComLibrary
        >
      + > ifneq ($(SNCSEQ),)
      + >     toto_SRCS += totoseq.st
      + >     toto_SRCS += tutuseq.st
      + >     toto_DBD += seq.dbd
      + >     toto_LIBS += seq pv
      + > endif
      + >
        > # Finally link to the EPICS Base libraries
        > toto_LIBS += $(EPICS_BASE_IOC_LIBS)
        >
        > #===========================
        >
        > include $(TOP)/configure/RULES
        > #----------------------------------------
        > #  ADD RULES AFTER THIS LINE
    ```

- On `make` :
    ```console
    $ make clean && make && echo OK || echo KO
    ```

- On modifie le `.cmd` pour appeler le programme SNL et on le lance :
    ```console
    $ cd iocBoot/ioctoto
    $ chmod +x st.cmd
    $ vi st.cmd
        > #!../../bin/linux-x86_64/toto
        >
        > #- You may have to change toto to something else
        > #- everywhere it appears in this file
        >
        > < envPaths
        >
        > cd "${TOP}"
        >
        > ## Register all support components
        > dbLoadDatabase "dbd/toto.dbd"
        > toto_registerRecordDeviceDriver pdbbase
        >
        > ## Load record instances
        > #dbLoadRecords("db/xxx.db","user=epicslearner")
      + > dbLoadRecords("db/toto.db","user=epicslearner")
        >
        > cd "${TOP}/iocBoot/${IOC}"
        > iocInit
        >
        > ## Start any sequence programs
        > #seq sncxxx,"user=epicslearner"
      + > seq totoseq, "plip_ou_plop=plop"
      + > seq tutuseq, "plip_ou_plop=plip"
    
    $ ./st.cmd
    ```

- Une fois dans l'IOC Shell on peut tester quelques commandes spécifiques à SEQ :
    ```console
    ...
    epics>  seqShow
            Program Name        Thread ID           Thread Name         SS Name
            ------------        ---------           -----------         -------
            tutuseq             0x56412a384ca0      tutuseq             ss1
            totoseq             0x56412a384260      totoseq             ss1
    
    epics>  seqShow toltoseq
            No such thread.
            epics> seqShow totoseq
            State Program: "totoseq"
              thread priority = 50
              number of state sets = 1
              number of syncQ queues = 0
              number of channels = 1
              number of channels assigned = 1
              number of channels connected = 1
              number of channels monitored = 1
              options: async=0, debug=0, newef=1, reent=0, conn=1
    
              State Set: "ss1"
              thread name = totoseq;  Thread id = 0x56412a384260
              First state = "init"
              Current state = "high"
              Previous state = "low"
              Elapsed time since state was entered = 58.80 seconds
              Wake up delay = inf seconds
              Get in progress = [0]
              Put in progress = [0]
    
    epics>  seqChanShow tutuseq
            State Program: "tutuseq"
            Number of channels=1
            View: State set ss1
    
            #0 of 1:
              Variable name: "v"
                type = double
                count = 1
              Value = 23
              Assigned to "foo:plip"
              Connected
              Monitored
              Not sync'ed
              Status = 17
              Severity = 0
              Message =
              Time stamp = <undefined>
            Next? (+/- skip count)
    
    epics>  seqcar 1
            Total programs=2, channels=2, connected=2, disconnected=0
    
    epics>  seqcar 2
              Program "tutuseq"
                Variable "v" connected to PV "foo:plip"
              Program "totoseq"
                Variable "v" connected to PV "foo:plop"
            Total programs=2, channels=2, connected=2, disconnected=0
    
    epics>  seqStop tutuseq
            sevr=info Instance 0 of sequencer program "tutuseq" terminated
    
    epics>  seq tutuseq,"plip_ou_plop=plip"
            sevr=info Sequencer release 2.2.9, compiled Mon Jul 25 12:02:24 2022
            sevr=info Spawning sequencer program "tutuseq", thread 0x56412a384ca0: "tutuseq"
            sevr=info tutuseq[0]: all channels connected & received 1st monitor
    ```

## Autres langages

> 5 mn

Comme dit durant la présentation, à mon avis le SNL ne devrait être utilisé que si :
- Une machine à états est incontestablement la meilleure approche pour résoudre le problème.
- Et qu’un simple client CA/PVA n’est pas suffisant (en utilisant un langage avec lequel vous êtes
  plus compétent qu'en SNL).
- Et qu’une solution “pur PVA” (par exemple en Python avec P4P) n’est pas envisageable si un
  serveur (IOC) est vraiment nécessaire (typiquement pour respecter la rétro-compatibilité CA de
  l’installation).

Si ces contraintes ne sont pas présentes, alors je vous recommande fortement d'utiliser un des projets suivants :

- C/C++
    - [PVXS](https://github.com/mdavidsaver/pvxs), PV Access en C++ (client et serveur)
        - recherchez `pvxs` dans <https://epics.anl.gov/tech-talk/search.php> pour plus de détails
    - [pvDataCPP](https://github.com/epics-base/pvDataCPP/) +
      [pvAccessCPP](https://github.com/epics-base/pvAccessCPP/) +
      [pvaClientCPP](https://github.com/epics-base/pvaClientCPP) +
      [normativeTypesCPP](https://github.com/epics-base/normativeTypesCPP) +
      [pvDatabaseCPP](https://github.com/epics-base/pvDatabaseCPP) +
      [pva2pva](https://github.com/epics-base/pva2pva) (client et serveur)
    - [exampleCPP](https://github.com/epics-rip/exampleCPP), exemples d'utilisation de PV Access en
      C++ (client et serveur)

- Python:
    - [p4p](https://github.com/mdavidsaver/p4p), **meilleur** module PV Access en Python (client et
      serveur)
        - recherchez `p4p` dans <https://epics.anl.gov/tech-talk/search.php> pour plus de détails
    - [pvaPy](https://github.com/epics-base/pvaPy), module PV Access  (client et serveur)
    - [PyEpics](https://github.com/pyepics/pyepics/) module Channel Access (client seulement)
    - [caproto](https://github.com/caproto/caproto) module Channel Access (client et serveur)

- Java
    - [exampleJava](https://github.com/epics-base/exampleJava), exemples PV Access en Java (client
      et serveur)
    - [JCA](https://github.com/epics-base/jca) module Channel Access (client et serveur)

⚠️ Je recommande fortement de ne pas utiliser de serveur Channel Access autre que celui de la base
EPICS ⚠️

La raison est plutôt simple : il est très difficile d'implémenter complètement la spécification
serveur du protocole Channel Access (il faut par exemple explicitement supporter tous les champs
EPICS et leurs subtilités). Je ne connais pas de projet qui le permet sans limitations, comparé à
la base (typiquement JCA et caproto ne supportent pas tous les champs).
